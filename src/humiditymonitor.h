/*
 * humiditymonitor.h
 *
 *  Created on: 6.6.2014
 *      Author: tomi
 */

#ifndef HUMIDITYMONITOR_H_
#define HUMIDITYMONITOR_H_

void* humidityMonitor(void* arg);

#endif /* HUMIDITYMONITOR_H_ */
