/*
 * humiditymonitor.c
 *
 *  Created on: 6.6.2014
 *      Author: tomi
 */


#include "main.h"
#include "humiditymonitor.h"

void* humidityMonitor(void* arg) {
	struct humidity_arg* data;
	data = (struct humidity_arg*) arg;

	while (*data->running)
	{
		pthread_mutex_lock(data->lock);

			if (*data->index < DATA_BUFFER_SIZE * HUMIDITY_THREAD_COUNT) {

				data->humidity[*data->index].data = rand();
				data->humidity[*data->index].timestamp = time(NULL);
				data->humidity[*data->index].sensor = data->sensor;

//				printf("Humiditythread #%d time %d humidity %d index %d\n",
//						data->sensor,
//						data->humidity[*data->index].timestamp,
//						data->humidity[*data->index].data,
//						*data->index);

				*data->index += 1;
			} else {
				printf("Humidity buffer is full!\n");
			}

		pthread_mutex_unlock(data->lock);
		sleep(MONITOR_SLEEP);
	}

	pthread_exit(NULL);
}
