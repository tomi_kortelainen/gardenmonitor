/*
 * soilmoisturemonitor.c
 *
 *  Created on: 6.8.2014
 *      Author: tomi
 */

#include "main.h"
#include "soilmoisturemonitor.h"


void* soilMoistureMonitor(void* arg)
{
	struct soil_moisture_arg* data;
	data = (struct soil_moisture_arg*) arg;

	while(*data->running)
	{
		pthread_mutex_lock(data->lock);

			if (*data->index < DATA_BUFFER_SIZE * SOIL_MOISTURE_THREAD_COUNT) {

				data->moisture[*data->index].data = rand();
				data->moisture[*data->index].timestamp = time(NULL);
				data->moisture[*data->index].sensor = data->sensor;

//				printf("Moistthread #%d time %d moist %d index %d\n",
//						data->sensor,
//						data->moisture[*data->index].timestamp,
//						data->moisture[*data->index].data,
//						*data->index);

				*data->index += 1;
			} else {
				printf("Moisture buffer is full!\n");
			}

		pthread_mutex_unlock(data->lock);
		sleep(MONITOR_SLEEP);
	}

	pthread_exit(NULL);
}
