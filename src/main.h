/*
 * main.h
 *
 *  Created on: 6.6.2014
 *      Author: tomi
 */

#ifndef MAIN_H_
#define MAIN_H_

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <time.h>
#include <pthread.h>
#include <sqlite3.h>
#include <bcm2835.h>

#define PIN1 RPI_GPIO_P1_11
#define PIN2 RPI_GPIO_P1_12
#define PIN3 RPI_GPIO_P1_24
#define PIN4 RPI_GPIO_P1_15
#define PIN5 RPI_GPIO_P1_16
#define PIN6 RPI_GPIO_P1_18
#define PIN7 RPI_GPIO_P1_22
//#define PIN8 RPI_GPIO_P1_07
#define PIN RPI_V2_GPIO_P1_13

#define LED1 PIN2
#define LED2 PIN5




#define DATABASE_NAME "collected_data.db"
#define TEMP_SENSOR1 "/sys/bus/w1/devices/28-0000048e42e7/w1_slave"
#define TEMP_SENSOR2 "/sys/bus/w1/devices/28-000005a9fe5c/w1_slave"
#define LED_TEMP 20000

#define TEMPERATURE_THREAD_COUNT   2
#define HUMIDITY_THREAD_COUNT      0
#define SOIL_MOISTURE_THREAD_COUNT 0
#define LED_THREAD_COUNT           2

#define MONITOR_SLEEP              2
#define MAIN_THREAD_SLEEP          8

#define DATA_BUFFER_SIZE 100

#define LED_ON  1
#define LED_OFF 0



struct item
{
	unsigned int timestamp;
	int sensor;
	int data;
};

struct collected_data
{
	int running;

	pthread_mutex_t temperature_lock;
	int temperature_index;
	struct item temperature_buffer[DATA_BUFFER_SIZE * TEMPERATURE_THREAD_COUNT];

	pthread_mutex_t humidity_lock;
	int humidity_index;
	struct item humidity_buffer[DATA_BUFFER_SIZE * HUMIDITY_THREAD_COUNT];

	pthread_mutex_t soil_moisture_lock;
	int soil_moisture_index;
	struct item soil_moisture_buffer[DATA_BUFFER_SIZE * SOIL_MOISTURE_THREAD_COUNT];

};

struct temperature_arg
{
	int* running;

	int sensor;
	char* path;
	int lednum;

	pthread_mutex_t* lock;
	int* index;
	struct item* temperature;
};

struct humidity_arg
{
	int* running;

	int sensor;

	pthread_mutex_t* lock;
	int* index;
	struct item* humidity;
};

struct soil_moisture_arg
{
	int* running;

	int sensor;

	pthread_mutex_t* lock;
	int* index;
	struct item* moisture;
};

#endif /* MAIN_H_ */
