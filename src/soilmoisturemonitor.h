/*
 * soilmoisturemonitor.h
 *
 *  Created on: 6.8.2014
 *      Author: tomi
 */

#ifndef SOILMOISTUREMONITOR_H_
#define SOILMOISTUREMONITOR_H_

void* soilMoistureMonitor(void* arg);

#endif /* SOILMOISTUREMONITOR_H_ */
