/*
 * temperaturemonitor.c
 *
 *  Created on: 6.6.2014
 *      Author: Tomi Kortelainen
 */

#include "main.h"
#include "temperaturemonitor.h"


void* temperatureMonitor(void* arg)
{
	struct temperature_arg* data;
	data = (struct temperature_arg*) arg;

	FILE *fp;

	while (*data->running)
	{
		pthread_mutex_lock(data->lock);

			if (*data->index < DATA_BUFFER_SIZE * TEMPERATURE_THREAD_COUNT) {

				fp = fopen(data->path, "r");

				char line[80];
				fgets(line, 80, fp);
				fgets(line, 80, fp);
				//printf("%d\n", atoi(line + 29));
				data->temperature[*data->index].data = atoi(line + 29);
				data->temperature[*data->index].timestamp = time(NULL);
				data->temperature[*data->index].sensor = data->sensor;

				if (data->temperature[*data->index].data > LED_TEMP) {
					bcm2835_gpio_write(data->lednum, HIGH);
				} else {
					bcm2835_gpio_write(data->lednum, LOW);
				}

//				printf("Tempthread #%d time %d temp %d index %d\n",
//						data->sensor,
//						data->temperature[*data->index].timestamp,
//						data->temperature[*data->index].data,
//						*data->index);

				*data->index += 1;

				fclose(fp);
			} else {
				printf("Temperature buffer is full!\n");
			}

		pthread_mutex_unlock(data->lock);
		sleep(MONITOR_SLEEP);
	}

	pthread_exit(NULL);
}
