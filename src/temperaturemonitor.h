/*
 * temperaturemonitor.h
 *
 *  Created on: 6.6.2014
 *      Author: tomi
 */

#ifndef TEMPERATUREMONITOR_H_
#define TEMPERATUREMONITOR_H_

void* temperatureMonitor(void* arg);

#endif /* TEMPERATUREMONITOR_H_ */
