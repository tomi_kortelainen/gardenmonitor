/*
 ============================================================================
 Name        : main.c
 Author      : Tomi Kortelainen
 Version     :
 Copyright   : (c) Copyright 2014 Tomi Kortelainen (tomi.kortelainen@gmail.com)
 Description : Garden Monitoring system.
 ============================================================================
 */

#include "main.h"
#include "temperaturemonitor.h"
#include "humiditymonitor.h"
#include "soilmoisturemonitor.h"

volatile int killed = 0;

void signalHandler(int sig);
static int sqlCallBack(void* NotUsed, int argc, char** argv, char** ColName);


int main(int argc, char* argv[])
{
	int t;  // Looping variable
	int rc; // return code temp variable

	char* temp_sensor_pahts[2] = {
			TEMP_SENSOR1,
			TEMP_SENSOR2
	};

	int lednums[2] = { LED1, LED2 };

	// Database variables
	sqlite3* database;
	char*    sqlErrMsg;
	char*    sqlCmd;

	bcm2835_init();

	bcm2835_gpio_fsel(LED1, BCM2835_GPIO_FSEL_OUTP);
	bcm2835_gpio_fsel(LED2, BCM2835_GPIO_FSEL_OUTP);

	// Thread arrays
	pthread_t temperature_threads[TEMPERATURE_THREAD_COUNT];
	pthread_t humidity_threads[HUMIDITY_THREAD_COUNT];
	pthread_t soil_moisture_threads[SOIL_MOISTURE_THREAD_COUNT];

	// Argument struct arrays for threads
	struct temperature_arg temperature_args[TEMPERATURE_THREAD_COUNT];
	struct humidity_arg    humidity_args[HUMIDITY_THREAD_COUNT];
	struct soil_moisture_arg soil_moisture_args[SOIL_MOISTURE_THREAD_COUNT];

	// Storage for collected data
	struct collected_data data;
	data.humidity_index = 0;
	data.temperature_index = 0;
	data.soil_moisture_index = 0;

	// TODO: Remove! Temporary for testing!!!
	srand(time(NULL));

	// Open and initialize database if needed
	if (access(DATABASE_NAME, F_OK) == -1) {

		rc = sqlite3_open(DATABASE_NAME, &database);
		if (rc) {
			printf("Can't open database: %s\n", sqlite3_errmsg(database));
			return(1);
		}

		printf("Creating database\n");
		sqlCmd = "CREATE TABLE TEMPERATURES (" \
				"ID INTEGER PRIMARY KEY," \
				"TIME INTEGER," \
				"TEMPERATURE INTEGER," \
				"SENSOR_ID INTEGER);" \
//
//				"CREATE TABLE MOISTURE (" \
//				"ID INTEGER PRIMARY KEY," \
//				"TIME INTEGER," \
//				"MOISTURE INTEGER," \
//				"SENSOR_ID INTEGER);"
				;
		rc = sqlite3_exec(database, sqlCmd, sqlCallBack, 0, &sqlErrMsg);
		if (rc != SQLITE_OK) {
			printf("Database not created\n");
		}
	} else {
		rc = sqlite3_open(DATABASE_NAME, &database);
		if (rc) {
			printf("Can't open database\n");
			return 1;
		}
		printf("Using existing database\n");
	}


	// Initialize mutex locks
	pthread_mutex_init(&data.temperature_lock, NULL);
	pthread_mutex_init(&data.humidity_lock, NULL);
	pthread_mutex_init(&data.soil_moisture_lock, NULL);
	data.running = 1;

	// Start threads for each temperature sensor
	for (t = 0; t < TEMPERATURE_THREAD_COUNT; t++)
	{
		pthread_mutex_lock(&data.temperature_lock);

			temperature_args[t].lock        = &data.temperature_lock;
			temperature_args[t].running     = &data.running;
			temperature_args[t].sensor      = t;
			temperature_args[t].index       = &data.temperature_index;
			temperature_args[t].temperature = &data.temperature_buffer[0];
			temperature_args[t].path = temp_sensor_pahts[t];
			temperature_args[t].lednum      = lednums[t];

			rc = pthread_create(&temperature_threads[t],
					NULL,
					temperatureMonitor,
					(void*)& temperature_args[t]
					);

			if (rc)
			{
				exit(-1);
			}

		pthread_mutex_unlock(&data.temperature_lock);
	}

	// Start threads for each humidity sensor
//	for (t = 0; t < HUMIDITY_THREAD_COUNT; t++)
//	{
//		pthread_mutex_lock(&data.humidity_lock);
//
//			humidity_args[t].lock        = &data.humidity_lock;
//			humidity_args[t].running     = &data.running;
//			humidity_args[t].sensor      = t;
//			humidity_args[t].index       = &data.humidity_index;
//			humidity_args[t].humidity    = &data.humidity_buffer[0];
//
//			rc = pthread_create(&humidity_threads[t],
//					NULL,
//					humidityMonitor,
//					(void*)& humidity_args[t]
//					);
//
//			if (rc)
//			{
//				exit(-1);
//			}
//
//		pthread_mutex_unlock(&data.humidity_lock);
//	}
//
//	// Start thread for each soil moisture sensor
//	for (t = 0; t < SOIL_MOISTURE_THREAD_COUNT; t++)
//	{
//		pthread_mutex_lock(&data.soil_moisture_lock);
//
//			soil_moisture_args[t].lock        = &data.soil_moisture_lock;
//			soil_moisture_args[t].running     = &data.running;
//			soil_moisture_args[t].sensor      = t;
//			soil_moisture_args[t].index       = &data.soil_moisture_index;
//			soil_moisture_args[t].moisture    = &data.soil_moisture_buffer[0];
//
//			rc = pthread_create(&soil_moisture_threads[t],
//					NULL,
//					soilMoistureMonitor,
//					(void*)& soil_moisture_args[t]
//					);
//
//			if (rc)
//			{
//				exit(-1);
//			}
//
//		pthread_mutex_unlock(&data.soil_moisture_lock);
//	}

	// Assign signal handler for SIGINT signal
	signal(SIGINT, signalHandler);

	// Main loop for main thread
	while (data.running)
	{
		int i;
		char sqlbuffer[1024];

		sleep(MAIN_THREAD_SLEEP);
		// Save temperatures to database
		pthread_mutex_lock(&data.temperature_lock);

			for (i = 0; i < data.temperature_index; i++) {
//				printf("INSERT INTO TEMPERATURES VALUES (NULL, '%d', '%d', '%d');\n",
//						data.temperature_buffer[i].timestamp,
//						data.temperature_buffer[i].data,
//						data.temperature_buffer[i].sensor);
				snprintf(sqlbuffer, sizeof(sqlbuffer),
						"INSERT INTO TEMPERATURES VALUES (NULL, '%d', '%d', '%d');",
						data.temperature_buffer[i].timestamp,
						data.temperature_buffer[i].data,
						data.temperature_buffer[i].sensor);
				rc = sqlite3_exec(database, sqlbuffer, sqlCallBack, 0, &sqlErrMsg);
				if (rc != SQLITE_OK) {
					printf("Can't save to the database\n");
					printf("error %s\n", sqlErrMsg);
				}
			}
			data.temperature_index = 0;
		pthread_mutex_unlock(&data.temperature_lock);

		// Save moisture data to database
//		pthread_mutex_lock(&data.soil_moisture_lock);
//			for (i = 0; i < data.soil_moisture_index; i++) {
//				snprintf(sqlbuffer, sizeof(sqlbuffer),
//						"INSERT INTO MOISTURE VALUES (NULL, '%d', '%d', '%d');",
//						data.soil_moisture_buffer[i].timestamp,
//						data.soil_moisture_buffer[i].data,
//						data.soil_moisture_buffer[i].sensor);
//				rc = sqlite3_exec(database, sqlbuffer, sqlCallBack, 0, &sqlErrMsg);
//				if (rc != SQLITE_OK) {
//					printf("jokin mättää\n");
//					printf("error %s\n", sqlErrMsg);
//				}
//			}
//			data.soil_moisture_index = 0;
//		pthread_mutex_unlock(&data.soil_moisture_lock);

		// Save humidity data to database (Not needed for now!)
//		pthread_mutex_lock(&data.humidity_lock);
//			for (i = 0; i < data.humidity_index; i++) {
//				printf("Humimon#%d time %d humi %d\n",
//						data.humidity_buffer[i].sensor,
//						data.humidity_buffer[i].timestamp,
//						data.humidity_buffer[i].data);
//
//			}
//			data.humidity_index = 0;
//		pthread_mutex_unlock(&data.humidity_lock);
		//printf("tsekki\n");
		if (killed)
			data.running = 0;
	}

	// All done, clean up everything!
	printf("\nCleaning up...\n");

	for (t = 0; t < TEMPERATURE_THREAD_COUNT; t++)
	{
		pthread_join(temperature_threads[t], NULL);
	}

	for (t = 0; t < HUMIDITY_THREAD_COUNT; t++)
	{
		pthread_join(humidity_threads[t], NULL);
	}

	pthread_mutex_destroy(&data.temperature_lock);
	pthread_mutex_destroy(&data.humidity_lock);

	bcm2835_gpio_write(LED1, LOW);
	bcm2835_gpio_write(LED2, LOW);
	bcm2835_close();

	sqlite3_close(database);
	printf("Done!\n");
	return EXIT_SUCCESS;
}

void signalHandler(int sig)
{
	if (sig == SIGINT)
		killed = 1;
}

static int sqlCallBack(void* NotUsed, int argc, char** argv, char** ColName)
{
	return 0;
}
