import web
import sqlite3
from datetime import datetime


render = web.template.render('templates/')
database_path = "collected_data.db"
db = sqlite3.connect(database_path)

urls = (
    '/', 'index'
)



class index:
    def GET(self):
        return getdata()

def getdata():
    items = []
    result = db.execute("select * from TEMPERATURES;").fetchall()
    html = "<html><head><title>GardenMonitor</title><head><body><ul>"
    for item in result:
        html += "<li>time: " + datetime.fromtimestamp(item[1]).isoformat() + " temperature: " + str(item[2] / 1000.0) + " sensor: " + str(item[3]) + "</li>\n"

    html += "</ul><body></html>"
    return html
        

if __name__ == "__main__":
    app = web.application(urls, globals())
    app.run()
