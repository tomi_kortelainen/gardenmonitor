# README #

Yksinkertainen ja keskeneräinen puutarhamonitori Raspberry Pi -alustalle.

Ohjelma kerää lämpötiloja kahdesta lämpötila-anturista ja tallentaa ne sqlite-tietokantaan.
Kummallekin anturille on omistettu ledi, joka syttyy jos lämpötila nousee yli 20 celsiusasteen.
Tiedosto "webserver.py" on esimerkki python-skriptistä, joka luo internetpalvelimen, joka näyttää kerätyn datan.
Tiedosto "harkka_bb.png" näyttää, kuinka kytkin komponentit koekytkentälautaan.

![harkka_bb.png](https://bitbucket.org/repo/xnXyez/images/2589393111-harkka_bb.png)

Tämä kansio on myös git-repository, joten git-työkaluilla voi nähdä kuinka työni edistyi.

Tomi Kortelainen
Kajaanin Ammattikorkeakoulu
TTI13SP
TTI12STomiK@kamk.fi